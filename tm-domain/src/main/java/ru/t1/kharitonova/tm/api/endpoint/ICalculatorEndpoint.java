package ru.t1.kharitonova.tm.api.endpoint;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import ru.t1.kharitonova.tm.dto.request.domain.*;
import ru.t1.kharitonova.tm.dto.response.domain.*;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;

@WebService
public interface ICalculatorEndpoint extends IEndPoint {

    @NotNull
    String NAME = "CalculatorEndpoint";

    @NotNull
    String PART = NAME + "Service";

    @SneakyThrows
    @WebMethod(exclude = true)
    static ICalculatorEndpoint newInstance() {
        return newInstance(HOST, PORT);
    }

    @SneakyThrows
    @WebMethod(exclude = true)
    static ICalculatorEndpoint newInstance(@NotNull final IConnectionProvider connectionProvider) {
        return IEndPoint.newInstance(connectionProvider, NAME, SPACE, PART, ICalculatorEndpoint.class);
    }

    @SneakyThrows
    @WebMethod(exclude = true)
    static ICalculatorEndpoint newInstance(@NotNull final String host, @NotNull final String port) {
        return IEndPoint.newInstance(host, port, NAME, SPACE, PART, ICalculatorEndpoint.class);
    }

    @WebMethod
    int sum(
            @WebParam(name = "a") int a,
            @WebParam(name = "b") int b
    );

    @WebMethod(exclude = true)
    static void main(String[] args) {
        System.out.println(ICalculatorEndpoint.newInstance().sum(100, 23));
    }

}
