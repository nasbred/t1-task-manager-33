package ru.t1.kharitonova.tm.dto.response.project;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.Nullable;
import ru.t1.kharitonova.tm.model.Project;

@NoArgsConstructor
public final class ProjectStartByIndexResponse extends AbstractProjectResponse {

    public ProjectStartByIndexResponse(@Nullable final Project project) {
        super(project);
    }

}
