package ru.t1.kharitonova.tm.dto.request.project;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.Nullable;
import ru.t1.kharitonova.tm.dto.request.AbstractIndexRequest;

@Getter
@Setter
@NoArgsConstructor
public final class ProjectStartByIndexRequest extends AbstractIndexRequest {

    public ProjectStartByIndexRequest(@Nullable final Integer index) {
        super(index);
    }

}

