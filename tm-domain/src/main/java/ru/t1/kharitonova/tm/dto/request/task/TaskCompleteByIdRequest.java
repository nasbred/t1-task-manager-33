package ru.t1.kharitonova.tm.dto.request.task;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.Nullable;
import ru.t1.kharitonova.tm.dto.request.AbstractIdRequest;

@Getter
@Setter
@NoArgsConstructor
public final class TaskCompleteByIdRequest extends AbstractIdRequest {

    public TaskCompleteByIdRequest(@Nullable final String id) {
        super(id);
    }

}
