package ru.t1.kharitonova.tm.dto.request.project;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.Nullable;
import ru.t1.kharitonova.tm.dto.request.AbstractIndexRequest;

@Getter
@Setter
@NoArgsConstructor
public final class ProjectUpdateByIndexRequest extends AbstractIndexRequest {

    @Nullable
    private String name;

    @Nullable
    private String description;

    public ProjectUpdateByIndexRequest(
            @Nullable final Integer index,
            @Nullable final String name,
            @Nullable final String description
    ) {
        super(index);
        this.name = name;
        this.description = description;
    }

}
