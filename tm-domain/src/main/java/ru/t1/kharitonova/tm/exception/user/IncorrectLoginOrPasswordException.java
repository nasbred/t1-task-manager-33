package ru.t1.kharitonova.tm.exception.user;

import ru.t1.kharitonova.tm.exception.AbstractException;

public class IncorrectLoginOrPasswordException extends AbstractException {

    public IncorrectLoginOrPasswordException() {
        super("Error! Incorrect Login or password.");
    }

}
