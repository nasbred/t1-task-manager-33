package ru.t1.kharitonova.tm.dto.response.user;

import ru.t1.kharitonova.tm.dto.response.AbstractResultResponse;

public final class UserLogoutResponse extends AbstractResultResponse {
}
