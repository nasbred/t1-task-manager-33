package ru.t1.kharitonova.tm.command.data;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.kharitonova.tm.dto.request.domain.DataJsonSaveJaxbRequest;
import ru.t1.kharitonova.tm.enumerated.Role;

public class DataJsonSaveJaxbCommand extends AbstractDataCommand {

    @Override
    @Nullable
    public String getArgument() {
        return null;
    }

    @Override
    @Nullable
    public String getDescription() {
        return "Save data to json file";
    }

    @Override
    @NotNull
    public String getName() {
        return "data-save-json-jaxb";
    }

    @SneakyThrows
    @Override
    public void execute() {
        System.out.println("[DATA SAVE JSON]");
        getDomainEndpoint().saveDataJsonJaxb(new DataJsonSaveJaxbRequest());
    }

    @Override
    @NotNull
    public Role[] getRoles() {
        return new Role[]{Role.ADMIN};
    }

}
