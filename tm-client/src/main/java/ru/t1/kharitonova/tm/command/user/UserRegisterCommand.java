package ru.t1.kharitonova.tm.command.user;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.kharitonova.tm.api.service.IAuthService;
import ru.t1.kharitonova.tm.dto.request.user.UserRegistryRequest;
import ru.t1.kharitonova.tm.enumerated.Role;
import ru.t1.kharitonova.tm.util.TerminalUtil;

public final class UserRegisterCommand extends AbstractUserCommand {

    @NotNull
    @Override
    public String getDescription() {
        return "Register user.";
    }

    @NotNull
    @Override
    public String getName() {
        return "user-register";
    }

    @Override
    public void execute() {
        System.out.println("[USER REGISTER]");
        System.out.println("ENTER LOGIN");
        @NotNull final String login = TerminalUtil.nextLine();
        System.out.println("ENTER PASSWORD");
        @NotNull final String password = TerminalUtil.nextLine();
        System.out.println("ENTER EMAIL");
        @NotNull final String email = TerminalUtil.nextLine();
        @NotNull final UserRegistryRequest request = new UserRegistryRequest(login, password, email);
    }

    @Nullable
    @Override
    public Role[] getRoles() {
        return null;
    }

}
