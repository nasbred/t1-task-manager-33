package ru.t1.kharitonova.tm.command.task;

import org.jetbrains.annotations.NotNull;
import ru.t1.kharitonova.tm.dto.request.task.TaskUpdateByIndexRequest;
import ru.t1.kharitonova.tm.util.TerminalUtil;

public final class TaskUpdateByIndexCommand extends AbstractTaskCommand {

    @NotNull
    @Override
    public String getDescription() {
        return "Update task by index.";
    }

    @NotNull
    @Override
    public String getName() {
        return "task-update-by-index";
    }

    @Override
    public void execute() {
        System.out.println("[UPDATE TASK BY INDEX]");
        System.out.println("ENTER INDEX:");
        @NotNull final Integer index = TerminalUtil.nextNumber() - 1;
        System.out.println("ENTER NAME");
        @NotNull final String name = TerminalUtil.nextLine();
        System.out.println("ENTER DESCRIPTION");
        @NotNull final String description = TerminalUtil.nextLine();
        @NotNull final TaskUpdateByIndexRequest request = new TaskUpdateByIndexRequest(index, name, description);
        getTaskEndpoint().updateTaskByIndex(request);
    }

}
