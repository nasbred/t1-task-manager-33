package ru.t1.kharitonova.tm.api.service;

import org.jetbrains.annotations.NotNull;
import ru.t1.kharitonova.tm.client.*;

public interface IServiceLocator {

    @NotNull
    ICommandService getCommandService();

    @NotNull
    IPropertyService getPropertyService();

    @NotNull
    ILoggerService getLoggerService();

    @NotNull
    TaskEndpointClient getTaskEndpoint();

    @NotNull
    AuthEndpointClient getAuthEndpoint();

    @NotNull
    ProjectEndpointClient getProjectEndpoint();

    @NotNull
    SystemEndpointClient getSystemEndpoint();

    @NotNull
    UserEndpointClient getUserEndpoint();

    @NotNull
    DomainEndpointClient getDomainEndpoint();

}
