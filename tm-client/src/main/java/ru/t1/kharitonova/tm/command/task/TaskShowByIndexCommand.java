package ru.t1.kharitonova.tm.command.task;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.kharitonova.tm.dto.request.task.TaskShowByIndexRequest;
import ru.t1.kharitonova.tm.model.Task;
import ru.t1.kharitonova.tm.util.TerminalUtil;

public final class TaskShowByIndexCommand extends AbstractTaskCommand {

    @NotNull
    @Override
    public String getDescription() {
        return "Show task by index.";
    }

    @NotNull
    @Override
    public String getName() {
        return "task-show-by-index";
    }

    @Override
    public void execute() {
        System.out.println("[SHOW TASK BY INDEX]");
        System.out.println("ENTER INDEX:");
        @NotNull final Integer index = TerminalUtil.nextNumber() - 1;
        @NotNull final TaskShowByIndexRequest request = new TaskShowByIndexRequest(index);
        @Nullable final Task task = getTaskEndpoint().showTaskByIndex(request).getTask();
        showTask(task);
    }

}
