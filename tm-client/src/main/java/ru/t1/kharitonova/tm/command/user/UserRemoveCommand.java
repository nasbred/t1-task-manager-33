package ru.t1.kharitonova.tm.command.user;

import org.jetbrains.annotations.NotNull;
import ru.t1.kharitonova.tm.dto.request.user.UserRemoveRequest;
import ru.t1.kharitonova.tm.enumerated.Role;
import ru.t1.kharitonova.tm.util.TerminalUtil;

public final class UserRemoveCommand extends AbstractUserCommand {

    @NotNull
    @Override
    public String getDescription() {
        return "Remove user.";
    }

    @NotNull
    @Override
    public String getName() {
        return "user-remove";
    }

    @Override
    public void execute() {
        System.out.println("[USER REMOVE]");
        System.out.println("ENTER LOGIN:");
        @NotNull final String login = TerminalUtil.nextLine();
        @NotNull final UserRemoveRequest request = new UserRemoveRequest(login);
        getUserEndpoint().removeUser(request);
    }

    @NotNull
    @Override
    public Role[] getRoles() {
        return new Role[]{Role.ADMIN};
    }

}
