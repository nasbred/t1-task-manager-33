package ru.t1.kharitonova.tm.command.task;

import org.jetbrains.annotations.NotNull;
import ru.t1.kharitonova.tm.dto.request.task.TaskUnbindFromProjectRequest;
import ru.t1.kharitonova.tm.util.TerminalUtil;

public final class TaskUnbindFromProjectCommand extends AbstractTaskCommand {

    @NotNull
    @Override
    public String getDescription() {
        return "Unbind task from project.";
    }

    @NotNull
    @Override
    public String getName() {
        return "task-unbind-from-project";
    }

    @Override
    public void execute() {
        System.out.println("[UNBIND TASK FROM PROJECT]");
        System.out.println("ENTER PROJECT ID: ");
        @NotNull final String projectId = TerminalUtil.nextLine();
        System.out.println("ENTER TASK ID: ");
        @NotNull final String taskId = TerminalUtil.nextLine();
        @NotNull final TaskUnbindFromProjectRequest request = new TaskUnbindFromProjectRequest(projectId, taskId);
        getTaskEndpoint().unbindTaskToProject(request);
    }

}
