package ru.t1.kharitonova.tm.api.endpoint;

import org.jetbrains.annotations.NotNull;
import ru.t1.kharitonova.tm.dto.request.task.*;
import ru.t1.kharitonova.tm.dto.response.task.*;

public interface ITaskEndpoint {

    @NotNull
    TaskChangeStatusByIdResponse changeStatusById(@NotNull TaskChangeStatusByIdRequest request);

    @NotNull
    TaskChangeStatusByIndexResponse changeStatusByIndex(@NotNull TaskChangeStatusByIndexRequest request);

    @NotNull
    TaskClearResponse clearTask(@NotNull TaskClearRequest request);

    @NotNull
    TaskCreateResponse createTask(@NotNull TaskCreateRequest request);

    @NotNull
    TaskListResponse listTask(@NotNull TaskListRequest request);

    @NotNull
    TaskListByProjectIdResponse listTaskByProjectId(@NotNull TaskListByProjectIdRequest request);

    @NotNull
    TaskShowByIdResponse showTaskById(@NotNull TaskShowByIdRequest request);

    @NotNull
    TaskShowByIndexResponse showTaskByIndex(@NotNull TaskShowByIndexRequest request);

    @NotNull
    TaskRemoveByIdResponse removeTaskById(@NotNull TaskRemoveByIdRequest request);

    @NotNull
    TaskRemoveByIndexResponse removeTaskByIndex(@NotNull TaskRemoveByIndexRequest request);

    @NotNull
    TaskUpdateByIdResponse updateTaskById(@NotNull TaskUpdateByIdRequest request);

    @NotNull
    TaskUpdateByIndexResponse updateTaskByIndex(@NotNull TaskUpdateByIndexRequest request);

    @NotNull
    TaskBindToProjectResponse bindTaskToProject(@NotNull TaskBindToProjectRequest request);

    @NotNull
    TaskUnbindFromProjectResponse unbindTaskToProject(@NotNull TaskUnbindFromProjectRequest request);

    @NotNull
    TaskStartByIdResponse startTaskById(@NotNull TaskStartByIdRequest request);

    @NotNull
    TaskStartByIndexResponse startTaskByIndex(@NotNull TaskStartByIndexRequest request);

    @NotNull
    TaskCompleteByIdResponse completeTaskById(@NotNull TaskCompleteByIdRequest request);

    @NotNull
    TaskCompleteByIndexResponse completeTaskByIndex(@NotNull TaskCompleteByIndexRequest request);

}
