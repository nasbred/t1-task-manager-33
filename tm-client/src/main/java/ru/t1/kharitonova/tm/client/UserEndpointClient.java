package ru.t1.kharitonova.tm.client;

import lombok.NoArgsConstructor;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import ru.t1.kharitonova.tm.api.endpoint.IEndpointClient;
import ru.t1.kharitonova.tm.api.endpoint.IUserEndpoint;
import ru.t1.kharitonova.tm.dto.request.user.*;
import ru.t1.kharitonova.tm.dto.response.user.*;

@NoArgsConstructor
public class UserEndpointClient extends AbstractEndpointClient implements IUserEndpoint, IEndpointClient {

    @NotNull
    @Override
    @SneakyThrows
    public UserLockResponse lockUser(@NotNull UserLockRequest request) {
        return call(request, UserLockResponse.class);
    }

    @NotNull
    @Override
    @SneakyThrows
    public UserUnlockResponse unlockUser(@NotNull final UserUnlockRequest request) {
        return call(request, UserUnlockResponse.class);
    }

    @NotNull
    @Override
    @SneakyThrows
    public UserRegistryResponse registryUser(@NotNull final UserRegistryRequest request) {
        return call(request, UserRegistryResponse.class);
    }

    @NotNull
    @Override
    @SneakyThrows
    public UserRemoveResponse removeUser(@NotNull final UserRemoveRequest request) {
        return call(request, UserRemoveResponse.class);
    }

    @NotNull
    @Override
    @SneakyThrows
    public UserUpdateProfileResponse updateUserProfile(@NotNull final UserUpdateProfileRequest request) {
        return call(request, UserUpdateProfileResponse.class);
    }

    @NotNull
    @Override
    @SneakyThrows
    public UserProfileResponse viewProfileUser(@NotNull final UserProfileRequest request) {
        return call(request, UserProfileResponse.class);
    }

    @NotNull
    @Override
    @SneakyThrows
    public UserChangePasswordResponse changeUserPassword(@NotNull final UserChangePasswordRequest request) {
        return call(request, UserChangePasswordResponse.class);
    }

}
