package ru.t1.kharitonova.tm.command.data;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.kharitonova.tm.dto.request.domain.DataJsonLoadJaxbRequest;
import ru.t1.kharitonova.tm.enumerated.Role;

public class DataJsonLoadJaxbCommand extends AbstractDataCommand {

    @Override
    @Nullable
    public String getArgument() {
        return null;
    }

    @Override
    @Nullable
    public String getDescription() {
        return "Load data from json file";
    }

    @Override
    @NotNull
    public String getName() {
        return "data-load-json-jaxb";
    }

    @SneakyThrows
    @Override
    public void execute() {
        System.out.println("[DATA LOAD JSON]");
        getDomainEndpoint().loadDataJsonJaxb(new DataJsonLoadJaxbRequest());
    }

    @Override
    @NotNull
    public Role[] getRoles() {
        return new Role[]{Role.ADMIN};
    }

}
