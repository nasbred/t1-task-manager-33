package ru.t1.kharitonova.tm.command;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.kharitonova.tm.api.model.ICommand;
import ru.t1.kharitonova.tm.api.service.IAuthService;
import ru.t1.kharitonova.tm.api.service.IServiceLocator;
import ru.t1.kharitonova.tm.enumerated.Role;

public abstract class AbstractCommand implements ICommand {

    protected IServiceLocator serviceLocator;

    @Nullable
    public abstract Role[] getRoles();

    @NotNull
    public IServiceLocator getServiceLocator() {
        return serviceLocator;
    }

    public void setServiceLocator(@NotNull final IServiceLocator serviceLocator) {
        this.serviceLocator = serviceLocator;
    }

    @NotNull
    @Override
    public String toString() {
        @Nullable final String name = getName();
        @Nullable final String argument = getArgument();
        @Nullable final String description = getDescription();
        @NotNull String result = "";
        if (name != null && !name.isEmpty()) result += name;
        if (argument != null && !argument.isEmpty()) result += " : " + argument;
        if (description != null && !description.isEmpty()) result += " : " + description;
        return result;
    }

}
