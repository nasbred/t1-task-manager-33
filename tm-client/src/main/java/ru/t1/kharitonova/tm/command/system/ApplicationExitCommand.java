package ru.t1.kharitonova.tm.command.system;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.kharitonova.tm.api.service.ILoggerService;

public final class ApplicationExitCommand extends AbstractSystemCommand {

    @Override
    public void execute() {
        @NotNull final ILoggerService loggerService = getServiceLocator().getLoggerService();
        loggerService.info("** EXIT TASK MANAGER **");
        System.exit(0);
    }

    @NotNull
    @Override
    public String getName() {
        return "exit";
    }

    @Nullable
    @Override
    public String getArgument() {
        return null;
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Close application";
    }

}
