package ru.t1.kharitonova.tm.command.data;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.kharitonova.tm.dto.request.domain.DataBinaryLoadRequest;
import ru.t1.kharitonova.tm.enumerated.Role;

public class DataBinaryLoadCommand extends AbstractDataCommand {

    @NotNull
    public static final String NAME = "data-load-bin";

    @Override
    @Nullable
    public String getArgument() {
        return null;
    }

    @Override
    @NotNull
    public String getDescription() {
        return "Load data from binary file";
    }

    @Override
    @NotNull
    public String getName() {
        return "data-load-bin";
    }

    @SneakyThrows
    @Override
    public void execute() {
        System.out.println("[DATA LOAD BINARY]");
        getDomainEndpoint().loadDataBinary(new DataBinaryLoadRequest());
    }

    @Override
    @NotNull
    public Role[] getRoles() {
        return new Role[]{Role.ADMIN};
    }

}
