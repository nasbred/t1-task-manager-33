package ru.t1.kharitonova.tm.client;

import lombok.NoArgsConstructor;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import ru.t1.kharitonova.tm.api.endpoint.IDomainEndpoint;
import ru.t1.kharitonova.tm.dto.request.domain.*;
import ru.t1.kharitonova.tm.dto.request.user.UserLoginRequest;
import ru.t1.kharitonova.tm.dto.request.user.UserLogoutRequest;
import ru.t1.kharitonova.tm.dto.response.domain.*;

@NoArgsConstructor
public class DomainEndpointClient extends AbstractEndpointClient implements IDomainEndpoint {

    public DomainEndpointClient(@NotNull final AbstractEndpointClient client) {
        super(client);
    }

    @NotNull
    @Override
    public DataBackupLoadResponse loadDataBackup(@NotNull final DataBackupLoadRequest request) {
        return call(request, DataBackupLoadResponse.class);
    }

    @NotNull
    @Override
    public DataBackupSaveResponse saveDataBackup(@NotNull final DataBackupSaveRequest request) {
        return call(request, DataBackupSaveResponse.class);
    }

    @NotNull
    @Override
    public DataBase64LoadResponse loadDataBase64(@NotNull final DataBase64LoadRequest request) {
        return call(request, DataBase64LoadResponse.class);
    }

    @NotNull
    @Override
    public DataBase64SaveResponse saveDataBase64(@NotNull final DataBase64SaveRequest request) {
        return call(request, DataBase64SaveResponse.class);
    }

    @NotNull
    @Override
    public DataBinaryLoadResponse loadDataBinary(@NotNull final DataBinaryLoadRequest request) {
        return call(request, DataBinaryLoadResponse.class);
    }

    @NotNull
    @Override
    public DataBinarySaveResponse saveDataBinary(@NotNull final DataBinarySaveRequest request) {
        return call(request, DataBinarySaveResponse.class);
    }

    @NotNull
    @Override
    public DataJsonLoadFasterXmlResponse loadDataJsonFasterXml(@NotNull final DataJsonLoadFasterXmlRequest request) {
        return call(request, DataJsonLoadFasterXmlResponse.class);
    }

    @NotNull
    @Override
    public DataJsonSaveFasterXmlResponse saveDataJsonFasterXml(@NotNull final DataJsonSaveFasterXmlRequest request) {
        return call(request, DataJsonSaveFasterXmlResponse.class);
    }

    @NotNull
    @Override
    public DataJsonLoadJaxbResponse loadDataJsonJaxb(@NotNull final DataJsonLoadJaxbRequest request) {
        return call(request, DataJsonLoadJaxbResponse.class);
    }

    @NotNull
    @Override
    public DataJsonSaveJaxbResponse saveDataJsonJaxb(@NotNull final DataJsonSaveJaxbRequest request) {
        return call(request, DataJsonSaveJaxbResponse.class);
    }

    @NotNull
    @Override
    public DataXmlLoadJaxbResponse loadDataXmlJaxb(@NotNull final DataXmlLoadJaxbRequest request) {
        return call(request, DataXmlLoadJaxbResponse.class);
    }

    @NotNull
    @Override
    public DataXmlSaveJaxbResponse saveDataXmlJaxb(@NotNull final DataXmlSaveJaxbRequest request) {
        return call(request, DataXmlSaveJaxbResponse.class);
    }

    @NotNull
    @Override
    public DataXmlLoadFasterXmlResponse loadDataXmlFasterXml(@NotNull final DataXmlLoadFasterXmlRequest request) {
        return call(request, DataXmlLoadFasterXmlResponse.class);
    }

    @NotNull
    @Override
    public DataXmlSaveFasterXmlResponse saveDataXmlFasterXml(@NotNull final DataXmlSaveFasterXmlRequest request) {
        return call(request, DataXmlSaveFasterXmlResponse.class);
    }

    @NotNull
    @Override
    public DataYamlLoadFasterXmlResponse loadDataYamlFasterXml(@NotNull final DataYamlLoadFasterXmlRequest request) {
        return call(request, DataYamlLoadFasterXmlResponse.class);
    }

    @NotNull
    @Override
    public DataYamlSaveFasterXmlResponse saveDataYamlFasterXml(@NotNull DataYamlSaveFasterXmlRequest request) {
        return call(request, DataYamlSaveFasterXmlResponse.class);
    }

}
